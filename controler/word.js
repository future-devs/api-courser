var router = require('express').Router();
var Word = require('./../model/schemas').getModel('Word');
var Lesson = require('./../model/schemas').getModel('Lesson');
var ObjectId = require('mongoose').Types.ObjectId;

router.route('/')
    .get(function(req, res) {
        query = {};

        if (req.query.search) {
            query = {
                $or: [
                    {
                        sideA: new RegExp(req.query.search)
                    },
                    {
                        sideB: new RegExp(req.query.search)
                    }
                ]
            }
        }
        console.log(query);

        Word.find(query).exec(function(err, words) {
            if (err) {
                res.send(err);
            }

            res.json(words);
        });
    });

router.route('/abandoned')
    .get(function(req, res) {
        query = {};

        if (req.query.search) {
            query = {
                $or: [
                    {
                        sideA: new RegExp(req.query.search)
                    },
                    {
                        sideB: new RegExp(req.query.search)
                    }
                ]
            }
        }
        query = {
            $and: [
                query,
                {
                    lesson: null
                }
            ]
        };
        console.log(query);

        Word.find(query).populate('lesson', 'name id').exec(function(err, words) {
            if (err) {
                res.send(err);
            }

            res.json(words);
        });
    });

router.route('/:id')
    .put(function(req, res) {

        Word.findById(req.params.id, function(err, word) {

            if (err) res.send(err);

            word.sideA = req.body.sideA;
            word.sideB = req.body.sideB;
            word.lesson = req.body.lesson;

            word.save(function(err) {
                if (err) res.send(err);

                res.json({ message: 'Word updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Word.remove({
            _id: req.params.id
        }, function(err, results) {
            if (err)
                res.send(err);
            Lesson.update({}, {$pull: {words: ObjectId(req.params.id)}},{ multi: true }, function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Word deleted' });
            });

        });
    });

module.exports = router;
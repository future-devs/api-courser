var router = require('express').Router();
var mongoose = require('mongoose');


var ObjectId = mongoose.Types.ObjectId;
var Lesson = require('./../model/schemas').getModel('Lesson');
var Word = require('./../model/schemas').getModel('Word');


router.route('/')
    .post(function(req, res) {

        var lesson = {};

        if (!req.body.name) {
            return res.status(400).json({errors: {name: 'Provide lesson name'}});
        }

        if (!req.body.content) {
            return res.status(400).json({errors: {content: 'Provide lesson content'}});
        }
        lesson._id = new ObjectId;
        lesson.name = req.body.name;
        lesson.content = req.body.content;
        lesson.words = [];

        req.body.words.forEach(word => {
            word._id = new ObjectId;
            word.lesson = lesson._id;
            lesson.words.push(word._id);
        });

        Word.create(req.body.words, function (err, docs) {
            if (err) {
                res.send(err);
            }

            Lesson.create(lesson, function(err, lesson) {
                if (err) {
                    res.send(err);
                }

                res.json({status: 'success', _id: lesson._id});
            });
        });
    })
    .get(function(req, res) {
        Lesson.find({}).populate('words').exec(function(err, lessons) {
            if (err) {
                res.send(err);
            }

            res.json(lessons);
        });
    });

router.route('/:id')
    .get(function(req, res) {
        Lesson.findById(req.params.id, function(err, lesson) {
            if (err) {
                res.send(err);
            }
            res.json(lesson);
        });
    })
    .put(function(req, res) {

        Lesson.findById(req.params.id, function(err, lesson) {

            if (err) res.send(err);

            lesson.name = req.body.name;
            lesson.content = req.body.content;

            lesson.save(function(err) {
                if (err) res.send(err);

                res.json({ message: 'Lesson updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Lesson.remove({
            _id: req.params.id
        }, function(err) {
            if (err)
                res.send(err);

            Word.update({lesson: req.params.id}, {lesson: null}, {multi: true}, function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'Successfully deleted' });
            });

        });
    });

module.exports = router;
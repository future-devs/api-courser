var express = require('express');
var app = express();
var bodyParser = require('body-parser');
require('./util/database'); //connect to database
require('./model/schemas').initModels();

app.set('port', (process.env.PORT || 5000));
app.use(require('./util/cors'));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

var router = express.Router();
router.get('/', function(req, res) {
    res.json({ message: 'API works' });
});
app.use('/', router);

app.use('/lessons', require('./controler/lesson'));
app.use('/words', require('./controler/word'));

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});
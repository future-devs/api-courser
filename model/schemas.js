var models = {};

module.exports = {
    getModel: function(name) {
        return models[name];
    },
    initModels: function() {
        models = {
            Lesson:  require('./lesson'),
            Word:  require('./word')
        };
    }
};
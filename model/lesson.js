var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LessonSchema = new Schema({
    name: String,
    content: String,
    words: [{ type: Schema.ObjectId, ref: 'Word'}]
});

module.exports = mongoose.model('Lesson', LessonSchema);
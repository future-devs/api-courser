var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WordSchema = new Schema({
    sideA: String,
    sideB: String,
    lesson: { type: Schema.ObjectId, ref: 'Lesson'}
});

module.exports = mongoose.model('Word', WordSchema);